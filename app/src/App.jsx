import React from 'react';
import Button from './components/Button';

class App extends React.Component {
	render() {
		return (
			<div style={{textAlign: 'center'}}>
				<h1 className='react-title'>React App - Setup</h1>
				<Button class='button--one' content='Click Me' />
			</div>
		);
	}
}

export default App;