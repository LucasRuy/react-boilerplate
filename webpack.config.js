const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
  template: './app/static/template/index.html',
  filename: 'index.html',
  inject: 'body'
})

module.exports = {
  entry:  {
    html:  './app/static/template/index.js',
    style: './app/static/assets/stylesheet/main.styl'
  },
  output: {
    path: path.resolve('dist'),
    filename: '[name].bundle.js'
  },
  module: {
    rules: [
      { 
        test: /\.js$/, 
        loader: 'babel-loader'
      },
      { 
        test: /\.jsx$/, 
        loader: 'babel-loader'
      },
      {
        test: /\.styl$/,
        loader: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader!stylus-loader'
        })
      },
    ]
  },
  plugins: [
    HtmlWebpackPluginConfig,
    new ExtractTextPlugin('assets/stylesheet/application.css')
  ]
}